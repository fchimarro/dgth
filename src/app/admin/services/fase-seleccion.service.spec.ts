import { TestBed } from '@angular/core/testing';

import { FaseSeleccionService } from './fase-seleccion.service';

describe('FaseSeleccionService', () => {
  let service: FaseSeleccionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FaseSeleccionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
