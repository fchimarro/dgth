'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">reclutamiento-front documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-bs-toggle="collapse" ${ isNormalMode ?
                                'data-bs-target="#modules-links"' : 'data-bs-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AdminModule.html" data-type="entity-link" >AdminModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-AdminModule-e5661b2c85f0313befb7fc51fe3504b9467b89621ef22124c4cf3b80f5fbb544ad263f3e73adc5352b2df419c168464e046443d76ba34f429f749c991fcdbacf"' : 'data-bs-target="#xs-components-links-module-AdminModule-e5661b2c85f0313befb7fc51fe3504b9467b89621ef22124c4cf3b80f5fbb544ad263f3e73adc5352b2df419c168464e046443d76ba34f429f749c991fcdbacf"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AdminModule-e5661b2c85f0313befb7fc51fe3504b9467b89621ef22124c4cf3b80f5fbb544ad263f3e73adc5352b2df419c168464e046443d76ba34f429f749c991fcdbacf"' :
                                            'id="xs-components-links-module-AdminModule-e5661b2c85f0313befb7fc51fe3504b9467b89621ef22124c4cf3b80f5fbb544ad263f3e73adc5352b2df419c168464e046443d76ba34f429f749c991fcdbacf"' }>
                                            <li class="link">
                                                <a href="components/FaseSeleccionCreateComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FaseSeleccionCreateComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FaseSeleccionListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FaseSeleccionListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FaseSeleccionUiComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FaseSeleccionUiComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FaseSeleccionUiListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FaseSeleccionUiListComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AdminRoutingModule.html" data-type="entity-link" >AdminRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-AppModule-f221a297ac92ab73dca83c114c0ec7bddff2498562face483a8d77dd8eea0ae445d6d9e9a100cf908845019079542af69b428457d9ca3420bfbb31ebc450e959"' : 'data-bs-target="#xs-components-links-module-AppModule-f221a297ac92ab73dca83c114c0ec7bddff2498562face483a8d77dd8eea0ae445d6d9e9a100cf908845019079542af69b428457d9ca3420bfbb31ebc450e959"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-f221a297ac92ab73dca83c114c0ec7bddff2498562face483a8d77dd8eea0ae445d6d9e9a100cf908845019079542af69b428457d9ca3420bfbb31ebc450e959"' :
                                            'id="xs-components-links-module-AppModule-f221a297ac92ab73dca83c114c0ec7bddff2498562face483a8d77dd8eea0ae445d6d9e9a100cf908845019079542af69b428457d9ca3420bfbb31ebc450e959"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link" >CoreModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialModule.html" data-type="entity-link" >MaterialModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-SharedModule-343689769c6dbb89186ca95dcc4dad555b98817895927f7c721c01c1094ebc55d3fbcfe13e69acaf6e719cdb5a29384c8a6ffd907aa7ad873547611841cde063"' : 'data-bs-target="#xs-components-links-module-SharedModule-343689769c6dbb89186ca95dcc4dad555b98817895927f7c721c01c1094ebc55d3fbcfe13e69acaf6e719cdb5a29384c8a6ffd907aa7ad873547611841cde063"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-343689769c6dbb89186ca95dcc4dad555b98817895927f7c721c01c1094ebc55d3fbcfe13e69acaf6e719cdb5a29384c8a6ffd907aa7ad873547611841cde063"' :
                                            'id="xs-components-links-module-SharedModule-343689769c6dbb89186ca95dcc4dad555b98817895927f7c721c01c1094ebc55d3fbcfe13e69acaf6e719cdb5a29384c8a6ffd907aa7ad873547611841cde063"' }>
                                            <li class="link">
                                                <a href="components/FooterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NavbarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ToolbarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ToolbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#interfaces-links"' :
                            'data-bs-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/FaseSeleccion.html" data-type="entity-link" >FaseSeleccion</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#miscellaneous-links"'
                            : 'data-bs-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank" rel="noopener noreferrer">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});