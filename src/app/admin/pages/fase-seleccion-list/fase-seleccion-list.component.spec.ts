import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseSeleccionListComponent } from './fase-seleccion-list.component';

describe('FaseSeleccionListComponent', () => {
  let component: FaseSeleccionListComponent;
  let fixture: ComponentFixture<FaseSeleccionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FaseSeleccionListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FaseSeleccionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
