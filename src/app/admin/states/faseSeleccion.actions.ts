import {createAction, props} from "@ngrx/store";
import {FaseSeleccion} from "../models/faseSeleccion.interface";

export enum FaseSeleccionActions {

  GET_FASESELECCION_LIST = '[FaseSeleccion] Get FaseSeleccion List',

  SET_FASESELECCION_LIST = '[FaseSeleccion] Set FaseSeleccion List',

  ADD_FASESELECCION_STATE = '[FaseSeleccion] Add FaseSeleccion (STATE)',

  ADD_FASESELECCION_API = '[FaseSeleccion] Add FaseSeleccion (API)',

  MODIFY_FASESELECCION_STATE = '[FaseSeleccion] Modify FaseSeleccion (STATE)',

  MODIFY_FASESELECCION_API = '[FaseSeleccion] Modify FaseSeleccion (API)',

  REMOVE_FASESELECCION_STATE = '[FaseSeleccion] Remove FaseSeleccion (STATE)',

  REMOVE_FASESELECCION_API = '[FaseSeleccion] Remove FaseSeleccion (API)',

}

export const getFaseSeleccionList = createAction(
  FaseSeleccionActions.GET_FASESELECCION_LIST);

export const setFaseSeleccionList = createAction(
  FaseSeleccionActions.SET_FASESELECCION_LIST,
  props<{faseSeleccions:FaseSeleccion[]}>()
)

export const addFaseSeleccionState = createAction(
  FaseSeleccionActions.ADD_FASESELECCION_STATE,
  props<{ faseSeleccions: FaseSeleccion }>()
)
export const updateFaseSeleccionState = createAction(
  FaseSeleccionActions.MODIFY_FASESELECCION_STATE,
  props<{ faseSeleccions: FaseSeleccion }>()
)

export const removeFaseSeleccionState = createAction(
  FaseSeleccionActions.REMOVE_FASESELECCION_STATE,
  props<{ faseSeleccionsId:number }>()
)

