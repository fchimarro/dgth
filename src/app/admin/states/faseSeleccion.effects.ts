import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {FaseSeleccionService} from "../services/fase-seleccion.service";
import {Router} from "@angular/router";
import {FaseSeleccionActions} from "./faseSeleccion.actions";
import {catchError, EMPTY, map, mergeMap, tap} from "rxjs";
import {FaseSeleccion} from "../models/faseSeleccion.interface";

@Injectable()
export class FaseSeleccionEffects {

  constructor(
    private actions: Actions,
    private faseSeleccionService: FaseSeleccionService,
    private router: Router
  ) {
  }

  faseSelecciones$= createEffect(() => {
    return this.actions.pipe(
      ofType(FaseSeleccionActions.GET_FASESELECCION_LIST),
      mergeMap(() => this.faseSeleccionService.getfaseSeleccion()
        .pipe(
          map(faseSelecciones => ({
            type: FaseSeleccionActions.SET_FASESELECCION_LIST,
            faseSelecciones
          })),
          catchError(() => EMPTY)
        ))
    )
  },);


  addFaseSeleccion$ = createEffect(() => {
      return this.actions.pipe(
        ofType(FaseSeleccionActions.ADD_FASESELECCION_API),
        mergeMap((data: {type: string, payload: FaseSeleccion}) => this.faseSeleccionService.addFaseSeleccion(data.payload)
            .pipe(
              map(faseSeleccion => ({
                type: FaseSeleccionActions.ADD_FASESELECCION_STATE,
                faseSeleccions: data.payload
              })),
              tap(() => this.router.navigate(["admin"])),
              catchError(() => EMPTY)
            ))
      )
    },);

  modifyFaseSeleccion$ = createEffect(() => {
    return this.actions.pipe(
      ofType(FaseSeleccionActions.MODIFY_FASESELECCION_API),
      mergeMap((data: {type: string, payload: FaseSeleccion}) =>
        this.faseSeleccionService.updateFaseSeleccion(data.payload.fasId, data.payload)
        .pipe(
          map(faseSeleccion => ({
            type: FaseSeleccionActions.MODIFY_FASESELECCION_STATE,
            faseSeleccions: data.payload
          })),
          tap(() => this.router.navigate(["admin"])),
          catchError(() => EMPTY)
        ))
    )
  },);


  removeFaseSeleccion$ = createEffect(() => {
    return this.actions.pipe(
      ofType(FaseSeleccionActions.REMOVE_FASESELECCION_API),
      mergeMap((data: {payload:number}) =>
        this.faseSeleccionService.deleteFaseSeleccion(data.payload)
          .pipe(
            map(faseSeleccion => ({
              type: FaseSeleccionActions.REMOVE_FASESELECCION_STATE,
              faseSeleccionsId: data.payload
            })),
            catchError(() => EMPTY)
          ))
    )
  },);
}
