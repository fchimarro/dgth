import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FaseSeleccion} from "../models/faseSeleccion.interface";
import {catchError, Observable, tap, throwError} from "rxjs";
import {environment} from "../../../environments/environment.development";

@Injectable({
  providedIn: 'root'
})
export class FaseSeleccionService {

  constructor(private http: HttpClient) {
  }
  getfaseSeleccion(): Observable<FaseSeleccion[]>{
    return this.http.get<FaseSeleccion[]>(`${environment.apiUrl}/reclutamiento`).pipe(
      tap((data: FaseSeleccion[]) =>data),
        catchError((err=>throwError(()=>err)))
    );
  }
getFaseSeleccion(id: number): Observable<FaseSeleccion>{
    return this.http.get<FaseSeleccion>(`${environment.apiUrl}/reclutamiento/${id}`).pipe(
      tap((data: FaseSeleccion)=>data),
      catchError((err=>throwError(()=>err)))
    )
}

addFaseSeleccion(faseSeleccion:FaseSeleccion):Observable<FaseSeleccion>{
    return this.http.post<FaseSeleccion>(`${environment.apiUrl}/reclutamiento/`, faseSeleccion).pipe(
      tap((data: FaseSeleccion)=>data),
      catchError((err=>throwError(()=>err)))
    )
}

updateFaseSeleccion(id:number, faseSeleccion:FaseSeleccion):Observable<FaseSeleccion>{
    return this.http.put<FaseSeleccion>(`${environment.apiUrl}/reclutamiento/${id}`, faseSeleccion).pipe(
      tap((data: FaseSeleccion)=>data),
      catchError((err=>throwError(()=>err)))
    )
}


deleteFaseSeleccion(id:number): Observable<FaseSeleccion>{
    return this.http.delete<FaseSeleccion>(`${environment.apiUrl}/reclutamiento/${id}`).pipe(
      catchError((err=>throwError(()=>err)))
    )
}
}
