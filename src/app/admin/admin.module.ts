import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FaseSeleccionCreateComponent} from "./pages/fase-seleccion-create/fase-seleccion-create.component";
import {SharedModule} from "../shared/shared.module";
import {FaseSeleccionUiComponent} from "./components/fase-seleccion-ui/fase-seleccion-ui.component";
import {MaterialModule} from "../material/material.module";
import {FaseSeleccionListComponent} from "./pages/fase-seleccion-list/fase-seleccion-list.component";
import {FaseSeleccionUiListComponent} from "./components/fase-seleccion-ui-list/fase-seleccion-ui-list.component";
import {ReactiveFormsModule} from "@angular/forms";
import {AdminRoutingModule} from "./admin-routing.module";

import { CardModule } from 'primeng/card';
import {InputTextModule} from "primeng/inputtext";
import {CalendarModule} from "primeng/calendar";
import {RadioButtonModule} from "primeng/radiobutton";
import { ToolbarModule } from 'primeng/toolbar';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ButtonModule } from 'primeng/button';
import {PrimengModule} from "../primeng/primeng.module";
import {StoreModule} from "@ngrx/store";
import {faseSeleccionReducer} from "./states/faseSeleccion.reducers";
import {EffectsModule} from "@ngrx/effects";
import {FaseSeleccionEffects} from "./states/faseSeleccion.effects";



@NgModule({
  /*
  * //Declramos todos los componentes
  * directivas pipes que forman parte del modulo se deben declarar en un solo modulo
  * */
  declarations: [
    FaseSeleccionCreateComponent,
    FaseSeleccionListComponent,
    FaseSeleccionUiComponent,
    FaseSeleccionUiListComponent
  ],
  imports: [ //Declaramos todos los modulos que van a utiliar
    CommonModule,
    SharedModule,
    MaterialModule,
    PrimengModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    StoreModule.forFeature('faseSeleccionState',faseSeleccionReducer),
    EffectsModule.forFeature([FaseSeleccionEffects])

  ],
  //Declaramos los servicios que van a estar disponibles para la inyeccion de dependencias
  providers:[],
  //Declramos todos los componentes directivas pipes que van a estar disponibles a otros modulos
  exports:[
    CardModule,
    InputTextModule,
    InputTextareaModule,
    ToolbarModule
  ],
  //Se define el componente principal del modulom esto es utilizado
  bootstrap: [],
  //Declaramos los componentesn que seberian ser cargados dinamicamente a la apliacion
  //entryComponents: [],
})
export class AdminModule { }
