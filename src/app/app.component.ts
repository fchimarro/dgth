import { Component } from '@angular/core';

@Component({
  selector: 'app-root', //como se va a llamar al componente
  templateUrl: './app.component.html', //cual es la pagina web o cual es el contenido html
  styleUrl: './app.component.css' //hojas de estilo
})
export class AppComponent {
  title = 'Sistema de Seleccion y Reclutamiento';
  nombre = 'Ejército Ecuatoriano';
  isDisabled = false;
}
