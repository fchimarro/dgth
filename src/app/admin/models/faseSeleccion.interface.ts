/*
*Clase que representa a la entidad Fase Seleccion
*
 */
export interface FaseSeleccion{
 fasId: number;
 fasDescripcion: string;
 fasFechareg:Date;
 fasObservacion:string;
 fasBorrado: boolean;
}
