import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseSeleccionUiListComponent } from './fase-seleccion-ui-list.component';

describe('FaseSeleccionUiListComponent', () => {
  let component: FaseSeleccionUiListComponent;
  let fixture: ComponentFixture<FaseSeleccionUiListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FaseSeleccionUiListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FaseSeleccionUiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
