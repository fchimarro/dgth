import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseSeleccionCreateComponent } from './fase-seleccion-create.component';

describe('FaseSeleccionCreateComponent', () => {
  let component: FaseSeleccionCreateComponent;
  let fixture: ComponentFixture<FaseSeleccionCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FaseSeleccionCreateComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FaseSeleccionCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
