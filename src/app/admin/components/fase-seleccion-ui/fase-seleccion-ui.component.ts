import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-fase-seleccion-ui',
  templateUrl: './fase-seleccion-ui.component.html',
  styleUrl: './fase-seleccion-ui.component.css'
})
export class FaseSeleccionUiComponent implements OnInit {
//Recibir informacion de los smart o components
  @Input() selectedId: string = "";
  @Input() actionButtonLabel: string = "Crear";

  //Enviar o ejecutar accion hacia los servicios
  @Output() action: EventEmitter<any> = new EventEmitter();

  //objeto para agrupar los elementos de un formulario utilizando reactives
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
    fasId: [''],
    fasDescripcion: [''],
    fasFechareg:[''],
    fasObservacion:[''],
    fasBorrado: ['']
    })
  }

  //Inicializa el formulario
  ngOnInit(): void {
    this.validarAction()
  }

  /**
    * @method para validar si se trata de una operacion de guardar o actualizar
   */
  validarAction() {
    if (this.selectedId) {
      this.actionButtonLabel = "Actualizar";
    }
  }

  emitirAction() {
    this.action.emit({value: this.form.value, action: this.actionButtonLabel})
  }

  clear() {
    this.form.reset()
  }
}
