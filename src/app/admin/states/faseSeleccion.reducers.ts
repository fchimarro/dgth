import {FaseSeleccion} from "../models/faseSeleccion.interface";
import {createReducer, on} from "@ngrx/store";
import {
  addFaseSeleccionState,
  removeFaseSeleccionState,
  setFaseSeleccionList,
  updateFaseSeleccionState
} from "./faseSeleccion.actions";

export interface FaseSeleccionState{
  faseSeleccions:FaseSeleccion[];
}

export const initialState:FaseSeleccionState ={
  faseSeleccions:[]
}

export const faseSeleccionReducer=createReducer(
  initialState,
  on(setFaseSeleccionList, (state,{faseSeleccions})=>{
    return{...state,faseSeleccions}
    }),
  on(addFaseSeleccionState,(state,{faseSeleccions})=>{
    return{...state, faseSeleccions:[...state.faseSeleccions, faseSeleccions]}
  }),
  on(updateFaseSeleccionState,(state,{faseSeleccions})=>{
    return{
      ...state,faseSeleccions:[...state.faseSeleccions.map(data=>
      data.fasId===faseSeleccions.fasId ? faseSeleccions:data)]
    }
    }),
  on(removeFaseSeleccionState,(state,{faseSeleccionsId})=>{
    return{
      ...state, fasId:[...state.faseSeleccions.filter(data=>data.fasId!= faseSeleccionsId)]
    }
  }),
);
