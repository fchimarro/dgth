import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {FaseSeleccionListComponent} from "./pages/fase-seleccion-list/fase-seleccion-list.component";
import {FaseSeleccionCreateComponent} from "./pages/fase-seleccion-create/fase-seleccion-create.component";


const routes:Routes=[
  {
    path: "",
    component:FaseSeleccionListComponent
  },
  {
    path: "faseSeleccion",
    component:FaseSeleccionCreateComponent
  }
]
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class AdminRoutingModule { }
