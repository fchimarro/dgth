import { Component } from '@angular/core';
import {AuthenticateService} from "../../../core/services/authenticate.service";
import {Store} from "@ngrx/store";
import {Router} from "@angular/router";
import {selectError, selectToken} from "../../state/auth.selectors";
import {User} from "../../models/user.interface";
import {AuthActions} from "../../state/auth.actions";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  error$=this.store.select(selectError);

  token$=this.store.select(selectToken);
  constructor(
    private store:Store,
    private authService:AuthenticateService,
    private router:Router
  ) {
    this.checkJWT();
    this.getError();
  }

  checkJWT(){
    if(this.authService.isAuthenticated()){
      this.router.navigate(["admin"]);
    }
  }
  getError(){
    this.error$.subscribe(data=>{
      if(data){

      }
    })
  }
  submit(data:User){
  this.store.dispatch({type:AuthActions.LOGIN, payload:data});
  this.setToken();
  }
  setToken() {
    this.token$.subscribe(data=>{
      if(data){
        localStorage.setItem('token',data);
      }

    })
  }
}
