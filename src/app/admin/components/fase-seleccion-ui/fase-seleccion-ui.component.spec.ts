import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseSeleccionUiComponent } from './fase-seleccion-ui.component';

describe('FaseSeleccionUiComponent', () => {
  let component: FaseSeleccionUiComponent;
  let fixture: ComponentFixture<FaseSeleccionUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FaseSeleccionUiComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FaseSeleccionUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
