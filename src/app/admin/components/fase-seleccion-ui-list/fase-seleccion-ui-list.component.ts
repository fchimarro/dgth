import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FaseSeleccion} from "../../models/faseSeleccion.interface";
import {Operaciones} from "../../enums/operaciones.enum";

@Component({
  selector: 'app-fase-seleccion-ui-list',
  templateUrl: './fase-seleccion-ui-list.component.html',
  styleUrl: './fase-seleccion-ui-list.component.css'
})
export class FaseSeleccionUiListComponent implements OnInit {

  @Input() cabeceras: Array<{
    cabecera: string, valorCabecera: keyof FaseSeleccion
  }> = [];

  @Input() faseSeleccions!: FaseSeleccion[];

  @Output() faseSeleccion: EventEmitter<any>= new EventEmitter<{faseSeleccion:FaseSeleccion, action:Operaciones}>();

  ngOnInit(): void {
  }

}
