import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-fase-seleccion-create',
  templateUrl: './fase-seleccion-create.component.html',
  styleUrl: './fase-seleccion-create.component.css'
})
export class FaseSeleccionCreateComponent implements OnInit {
  //Variable para poder identificar si se va a guardar o actualizar
  id: string="";
  constructor(private router: ActivatedRoute) {}
  ngOnInit(): void {
  this.id = this.router.snapshot.params['id'];
  }


}
