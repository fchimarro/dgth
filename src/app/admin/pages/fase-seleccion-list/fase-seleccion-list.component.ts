import {Component, EventEmitter, OnInit} from '@angular/core';
import {FaseSeleccion} from "../../models/faseSeleccion.interface";
import {Router} from "@angular/router";
import {Operaciones} from "../../enums/operaciones.enum";

@Component({
  selector: 'app-fase-seleccion-list',
  templateUrl: './fase-seleccion-list.component.html',
  styleUrl: './fase-seleccion-list.component.css'
})
export class FaseSeleccionListComponent implements OnInit {

  tituloFormulario: string = "Gestion de Fase";

  faseSeleccions!: FaseSeleccion[];

  faseSeleccion: EventEmitter<any>= new EventEmitter<{faseSeleccion:FaseSeleccion, action:Operaciones}>();

  cabeceras: {
    cabecera: string,
    valorCabecera: keyof FaseSeleccion
  }[] = [
    {cabecera: "Id", valorCabecera: "fasId"},
    {cabecera: "Descripcion", valorCabecera: "fasDescripcion"},
    {cabecera: "Fecha Registro", valorCabecera: "fasFechareg"},
    {cabecera: "Observacion", valorCabecera: "fasObservacion"},
  ]
constructor(
  private router: Router,
) {}

  ngOnInit(): void {
  }
  seleccionarFase (data:{faseSeleccion:FaseSeleccion, action:Operaciones}){

  }
}
