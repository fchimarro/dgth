import {FaseSeleccionState} from "./faseSeleccion.reducers";
import {createFeatureSelector, createSelector} from "@ngrx/store";


export const selectFaseSeleccionState =
  createFeatureSelector<FaseSeleccionState>('FaseSeleccionState');

export const selectFaseSelecciones=()=>createSelector(
    selectFaseSeleccionState,
    (state:FaseSeleccionState)=>state.faseSeleccions
  )

export const selectFaseSeleccion=(id:number)=>createSelector(
  selectFaseSeleccionState,
  (state:FaseSeleccionState)=>state.faseSeleccions.find(d=>d.fasId == id)
)
