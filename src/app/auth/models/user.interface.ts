export interface User {
  usuId: number;
  usuNombre: string;
  usuClave: string;
  usuFeccreacion: string;
  usuCorreo: string;
  token: string;
}
